# Use an official Python runtime as a parent image
FROM python:3.8-slim
# FROM openjdk:8-jdk
WORKDIR /app
COPY . /app

# go in the app directory and extract the kakfa tar file
# RUN tar -xzf kafka_2.12-2.5.0.tgz
# go in the kafka directory
# WORKDIR /app/kafka_2.12-2.5.0
# set the JAVA_HOME environment variable
# set the PATH environment variable
# ENV PATH $PATH:$JAVA_HOME/bin
# RUN ./gradlew jar -PscalaVersion=2.13.11
# start zookeeper and kafka server
# RUN bin/zookeeper-server-start.sh config/zookeeper.properties &
# RUN bin/kafka-server-start.sh config/server.properties &
# create a topic
# RUN bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test
# go back to the app directory
WORKDIR /app
# install pip
# RUN apt-get update && apt-get install -y python3-pip
RUN pip install --trusted-host pypi.python.org -r requirements.txt
