from confluent_kafka import Consumer

consumer = Consumer({
    'bootstrap.servers': 'my-kafka:9092',
    'group.id': 'mygroup',
    'auto.offset.reset': 'earliest'
})

def register_consumer():
    consumer.subscribe(['registration'])

    msg = consumer.poll(1.0)

    if msg is None:
        print("No message received")
        return
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        return
    print('Received message: {}'.format(msg.value().decode('utf-8')))

def otp_validation_consumer():
    consumer.subscribe(['otp_validation'])

    while True:
        msg = consumer.poll(1.0)
        if msg is None:
            print("No message received otp_validation")
            continue
        if msg.error():
            print("Consumer error: {}".format(msg.error()))
            continue
        print('Received message: {}'.format(msg.value().decode('utf-8')))
